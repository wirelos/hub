function docker_login() {
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $DOCKER_REGISTRY
}

function build_image() {
    local TAG=$DOCKER_IMAGE:$1
    local DOCKERFILE=$2
    local CONTEXT=$3
    docker build --pull -t $TAG -f $DOCKERFILE $CONTEXT
}

function push_image() {
    docker push $DOCKER_IMAGE:$1
}

function release_image() {
    local TAG=$DOCKER_IMAGE:$1
    local TAG_RELEASE=$DOCKER_IMAGE:$2

    docker pull $TAG
    docker tag $TAG $TAG_RELEASE
    docker push $TAG_RELEASE
}
