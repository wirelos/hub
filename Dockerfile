# Hugo build
FROM registry.gitlab.com/pages/hugo:0.65.3 as build
WORKDIR /src
COPY . /src
RUN hugo

# Hugo serve dev
FROM build as serve-dev
CMD  hugo server --source=/src --bind=0.0.0.0

# Hugo serve
FROM build as serve
CMD  hugo server --watch=false --disableLiveReload --source=/src --bind=0.0.0.0

# nginx server for prod
# nginx Docker image is much bigger then hugo...
FROM nginx:1.17 as serve-prod
COPY --from=build /src/public /usr/share/nginx/html
