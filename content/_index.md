### Federation
<a class="app-link" href="#" target="_blank"><i class="fas fa-cloud"></i><span>NextCloud</span></a>
<a class="app-link" href="#" target="_blank"><i class="fab fa-hubspot"></i><span>HubZilla</span></a>
<a class="app-link" href="#" target="_blank"><i class="far fa-images"></i><span>PixelFed</span></a>
<a class="app-link" href="#" target="_blank"><i class="fas fa-music"></i><span>FunkWhale</span></a>
<a class="app-link" href="#" target="_blank"><i class="fas fa-comments"></i><span>NodeBB</span></a>
<a class="app-link" href="#" target="_blank"><i class="fas fa-hashtag"></i><span>Pleroma</span></a>
<a class="app-link" href="#" target="_blank"><i class="fas fa-sync-alt"></i><span>Syncthing</span></a>

### IoT
<a class="app-link" href="#" target="_blank"><i class="fas fa-chart-line"></i><span>Node-RED Dashboard</span></a>
<a class="app-link" href="#" target="_blank"><i class="fas fa-project-diagram"></i><span>Node-RED Flows</span></a>
<a class="app-link" href="#" target="_blank"><i class="fab fa-connectdevelop"></i><span>MQTT Broker</span></a>